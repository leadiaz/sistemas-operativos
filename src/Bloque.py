class Bloque():
	def __init__(self,base, limite):
		self._base = base
		self._limite = limite
		self._nextBloque = None
		self._estaOcupado = False
		self._bit = False
	def estaOcupado(self):
		return self._estaOcupado
	def setEstaOcupado(self, boolean):
		self._estaOcupado = boolean

	def setNextBloque(self, bloque):
		self._nextBloque = bloque

	def setLimite(self, limit):
		self._limite = limit

	def size(self):
		return self._limite - self._base

	def nextBloque(self):
		return self._nextBloque

	def getLimite(self):
		return self._limite	

	def setBd(self, bd):
		self._base =	bd 
	def getBd(self):
		return self._base

	def hasNext(self):
		return self._nextBloque != None
	def bit(self):
		return  self._bit
	def setBit(self, boolean):
		self._bit  =boolean
	def __repr__(self):
		return "dir base: " + str(self._base) + " ,limite: " + str(self._limite) + " ,bit: " + str(self._bit)