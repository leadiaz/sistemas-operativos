class Clock(object):
	
	def __init__(self, cpu, ioDevice):
		self._cpu = cpu
		self._IoDevice = ioDevice
		self._scheduler = None

	def setSchedulerRoundRobin(self, scheduler):
		self._scheduler = scheduler
			
	def tick(self):
			self._cpu.tick()
			self._IoDevice.tick()
			if  self._scheduler != None:
				self._scheduler.tick()
			

class ClockRoundRobin(Clock):
	"""docstring for ClassName"""
	def __init__(self, arg):
		super(ClockRoundRobin, self).__init__()
		self._scheduler = arg
		
	def tick(self):
		super(ClockRoundRobin,self).tick()
		#if self._cpu.estaEnUso():
		#	self._scheduler.tick()


