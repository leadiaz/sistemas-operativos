from time import sleep
import logging

from so import *

class Cpu():

    def __init__(self):
        self._esPaginacion = False
        self._memory = None
        self._pc = 0
        self._ir = None
        self.logger = logging.getLogger()
        self._interruptManager = None
        self._estaEnUso = False
        self._mmu = None
    def tick(self):
        if self._esPaginacion:
            if self._estaEnUso:
                self._fetch()
                self._decode()
                self._execute()
        else:
            self._fetch()
            self._decode()
            self._execute()
                    
    def setearEsPaginacion(self, boole):
        self._esPaginacion = boole        
    def _fetch(self):
        #self._ir = self._memory.fetch(self._pc)
        #self._ir = self._memory.fetch(self._mmu.getBd()+self._pc)
        self._ir = self._mmu.fetch(self._pc)
        self._pc += 1

    def _decode(self):
        # el decode no hace nada en este emulador
        pass
    def estaEnUso(self):
        return self._estaEnUso
    def setEstaEnUso(self, bolean):
        self._estaEnUso = bolean 
    def setMmu(self, mmu):
        self._mmu = mmu                 
    def getMemory(self):
        return self._memory    
    def _execute(self):
        if self._ir.isCpu():
            self.logger.debug("Exec: {op}, PC={pc}".format(op=self._ir, pc=self._pc))
            #self._interruptManager.getDispatcher().modificarSize()
            #dself._interruptManager.getScheduler().tick()
            sleep(0.3)
        elif self._ir.isIo():
            print ("interrupcion de io : " + str(self._pc))
            self._interruptManager.io()
        else:
            print ("proceso terminado : " + str(self._pc))
            self._interruptManager.kill()
            

    def interruptManager(self):
        return self._interruptManager

    def setInterruptManager(self, interrupt):
        self._interruptManager = interrupt
    def setMemory(self,memory):
        self._memory = memory        

    @property
    def pc(self):
        return self._pc

    def getPc(self):
        return self._pc 
    def setPc(self,pc):
        self._pc = pc     

    @pc.setter
    def pc(self, addr):
        self._pc = addr


    def __repr__(self):
        return "CPU(PC={pc})".format(pc=self._pc)