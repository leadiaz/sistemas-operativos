class Disk(object):

    def __init__(self):
        self._datos = []

    def getProgram(self, name):
        for p in self._datos:
            if (p.name == name):
                return p


    def addDatos(self, dato):
        self._datos.append(dato)


    def removeDato(self, dato):
        self._datos.remove(dato)

    def getData(self):
        return self._datos

    def len(self):
        return len(self._datos)
