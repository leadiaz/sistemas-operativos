class Dispatcher():

	def __init__(self):
		self._pcbActual = None
		self._cpu= None
		self._pcbAnterior = None;
		self._MMU = None

	def mmu(self):
		return self._MMU
		
	def pcbActual(self):
		return self._pcbActual

	def setCpu(self, cpu):
		self._cpu = cpu

	def setMmu(self, mmu):
		self._MMU = mmu
			
	def load(self, pcb):
		## carga el pcb en el cpu
		pcb.setState("RUNNING")
		#self._cpu.pc = pcb.getPc()
		self._cpu.setPc(pcb.getPc())
		#print("Loader PC a Cargar:" + str(pcb.getPc()))
		self._pcbActual = pcb
		#self._MMU.setDB(self._MMU.getBd()+1) ## deprecado usar el de abajo
		self._MMU.setPcb(pcb)
		self._MMU.setDB(pcb.getBd()) ## 
		self._cpu.setEstaEnUso(True)

	def save(self):
		self._pcbActual.setPc(self._cpu.pc)
		return self._pcbActual
	def modificarSize(self):
		self._pcbActual.setSize(self._pcbActual.size() - 1)
	def getCpu(self):
		return self._cpu
