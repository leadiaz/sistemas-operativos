class IODevice(object):
	def __init__(self):
		self._queueWaiting = []
		self._currentPCB  = None
		self._runTime = 3
		self._tickCount = 0
		self._interrumpManager = None

	def tick(self):
		if self._currentPCB != None:
			self.keepRunning()
		else:
			self.loadNextQueueWaiting()			 	
	def add(self, pcb):
		self._queueWaiting.append(pcb)

	def keepRunning(self):
		self._tickCount = self._tickCount + 1
		#print(self._tickCount)
		#print(self._runTime)
		#print(self._currentPCB.id())
		if self._runTime < self._tickCount:
			self._interrumpManager.completeIO(self._currentPCB)
			self._currentPCB = None
	def loadNextQueueWaiting(self):
		if self._queueWaiting != []:
			self._currentPCB = self._queueWaiting.pop(0)
			self._tickCount = 1
	def getPCB(self):
		return self._currentPCB
	def queueWaiting(self):
		return self._queueWaiting

	def setInterrumpManager(self, manager):
		self._interrumpManager = manager
	def getTickCount(self):
		return self._tickCount		

