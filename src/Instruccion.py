from time import sleep
import logging
class Instr():


    def __init__(self, count):
        ## Constructor de la clase Instr con el count como atributo (dicho count es la cada de instrucciones de esa clase )
        self._count = count

    def isExit(self):
        return False

    @property
    def count(self):
        return self._count

    def expand(self):
        expanded = []#[self.__class__(0)]

        for _ in range(self._count):
            expanded.append(self.__class__(0))

        return expanded

    def isCpu(self):
        return False

    def isIo(self):
        return False


class CPU(Instr):

    def __repr__(self):
        if self._count:
            return "CPU({count})".format(count=self._count)
        else:
            return "CPU"

    def isCpu(self):
        return True

class IO(Instr):

    def __repr__(self):
        if self._count:
            return "IO({count})".format(count=self._count)
        else:
            return "IO"
    def isIo(self):
        return True


class EXIT(Instr):

    def isExit(self):
        return True

    def __repr__(self):
        return "EXIT"
