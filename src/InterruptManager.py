from Pcb import  *
from PcbTable import *
from Cpu import *
from IODevice import *
from Loader import *
from Scheduler import *

class InterruptManager():
	
	def __init__(self, pcbTable, loader):
		self._dispatcher = None
		self._schedule = None
		self._cpu = None
		self._pcbTable = pcbTable
		self._ioDevice = None
		self._loader =  loader
		self._memoryManager = None
	@property
	def pcbTable(self):
		return self._pcbTable
	@pcbTable.setter
	def pcbTable(self,pcbT):
		self._pcbTable = pcbT

	@property
	def loader(self):
		return self._loader
	@loader.setter
	def loader(self, loader):
		self._loader = loader
	@property
	def ioDevice(self):
		return self._ioDevice

	def setIoDevice(self, io):
		self._ioDevice = io
		
	def getDispatcher(self):
		return self._dispatcher
	def setDispatcher(self, dispatcher):
		self._dispatcher = dispatcher

	def getScheduler(self):
		return self._schedule
	def setScheduler(self, schedule):
		self._schedule = schedule	

	def getCpu(self):
		return self._cpu
	def setCpu(self, cpu):
		self._cpu =	cpu
	def setMemorymanager(self, memorymanager):
		self._memoryManager = memorymanager

				

	def new(self, program, prioridad):
		if self._memoryManager.puedeCargar(program):
			pcb = Pcb(self._pcbTable.nextID(),self._dispatcher.mmu().getBd(),prioridad,program.size)
			#print(pcb.getPc())
			print("Agregado un nuevo proceso Con ID:" + str(pcb.id()))
			pcb.setState("READY")
			self._pcbTable.add(pcb)
			self._memoryManager.load(program, pcb)
			if not self._cpu.estaEnUso():
				self._dispatcher.load(pcb)
			elif self._schedule.esExpropiativo():
				pcb2 = self._schedule.compararOrden(pcb, self._dispatcher.pcbActual())
				if pcb.id() == pcb2.id():
					pcbAct = self._dispatcher.save()
					pcbAct.setState("READY")
					self._schedule.add(pcbAct)
					self._dispatcher.load(pcb2)
					print("Switch Scheduler Expropiativo " + str(pcb2))
				else:
					self._schedule.add(pcb)
			else:
				self._schedule.add(pcb)
		else:
			print ("Memoria insuficiente")




	def io(self):
		pcb = self._dispatcher.save()
		pcb.setState("WAITING")
		print("PCB IO :" + str(pcb))
		self._pcbTable.modificarPcb(pcb)
		self._ioDevice.add(pcb)
		if self._schedule.hasNext():
			pcbnext = self._schedule.getNext()
			self._dispatcher.load(pcbnext)
			#print("PCB IO NEXT PC:" + str(pcbnext.getPc()))
		else:

			print("NO HAY NEXT ")	

	def kill(self):
		pcb = self._dispatcher.save()
		pcb.setState("Terminated")
		self._pcbTable.modificarPcb(pcb)
		print("Tiempo De Espera:"+ str(pcb.getTiempoEspera()))
		siguiente = False
		if self._schedule.hasNext():
			self._dispatcher.load(self._schedule.getNext())
			siguiente = True
		else:
		#print(self._schedule.getNext().id())
		#print(self._schedule.getNext().getBd())
		#print(self._schedule.getNext().getPc())
			self._dispatcher.getCpu().setEstaEnUso(False)
			print("No hay Mas Procesos A Cargar")
			print("Tiempo Promedio de Espera: " + str(self._pcbTable.tiempoEsperaPromedio()))
		self._loader.clear(pcb)

		print(str(pcb))
		print(self._cpu.getMemory())
		if siguiente:
			print("Siguiente Proceso A Cargar: " +str(self._dispatcher.pcbActual())+", Id: "+str(self._dispatcher.pcbActual().id()))		

	def timeOut(self):
		pcb = self._dispatcher.save()
		pcb.setState("READY")
		#print("PCB timeOut Pc guardado:" + str(pcb.getPc()))
		self._schedule.add(pcb)
		if self._schedule.hasNext():
			pcbNext= self._schedule.getNext()
			self._dispatcher.load(pcbNext)
			print("Round Robin Interrump:" + str(pcbNext))
		else: "NO HAY SIGUIENTE En El Scheduler"
		#print("Round Robin Interrump PC:" + str(self._schedule.getNext().getPc()))

	def completeIO(self,pcb):

		pcb.setState("READY")
		#print(pcb.getPc())
		#print(pcb.size())
		if self._schedule.esExpropiativo():
			pcb2 = self._schedule.compararOrden(pcb, self._dispatcher.pcbActual())
			if pcb.id() == pcb2.id():
				pcbAct = self._dispatcher.save()
				pcbAct.setState("READY")
				self._schedule.add(pcbAct)
				self._dispatcher.load(pcb2)
				print("Switch Scheduler Expropiativo IO Completado " + str(pcb2))	
			else:
				self._schedule.add(pcb)	
		else:
			self._schedule.add(pcb)
			#if pcb.getPc() == pcb.size:
			#	print("LALO")
			#else:
			#	self._schedule.add(pcb)

	def pageFault(self):
		pcb = self._dispatcher.save()
		pcb.setState("READY")
		self._loader.clear(pcb)

		self._memoryManager.loadPageFault(pcb)
		#self._schedule.add(pcb)
		print (pcb)
		if self._schedule.hasNext():
			pcbNext= self._schedule.getNext()
			self._dispatcher.load(pcbNext)
		else:
			self._dispatcher.load(pcb)
		print ("page fault")





		
		
		
