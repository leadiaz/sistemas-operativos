from Bloque import *
class Loader():
	"""docstring for ClassName"""
	def __init__(self,memory, disk, mmu ):
		self._memory = memory
		self._disk = disk
		self._mmu = mmu
		self._busquedaDeBloque = None
		self._memoryManager = None  
		self._pcbTable = None
	def setMemory(self, memo):
		self._memory = memo;
	def setPcbTable(self, ta):
		self._pcbTable = ta	

	def setMemoryManager(self, memoryManager):
		self._memoryManager = memoryManager

	def load(self, program, pcb):
		programa = self._disk.getProgram(program.name)
		##self._memoryManager.bloqueAsignado(programa, self._mmu.getBd(), pcb)
		#for i in programa.instructions: ##           {
		#	self._memory.getMemory().append(i) ##    DEPRECATED
		#	self._mmu.setDB(self._mmu.getBd()+1) ##   }

		#self._memory = self._memory.load(programa.instructions())
		self._memoryManager.load(programa, pcb)

	
	def loadAsigConti(self,bdBloque,programa, pcb):
		pcb.setBd(bdBloque)
		for i in programa.instructions: 
			#self._memory.getMemory().insert(bdBloque, i)
			self._memory.setIndexMemory(bdBloque,i)
			##self._mmu.setDB(self._mmu.getBd()+1)
			bdBloque = bdBloque +1 
	def desarmarBloqueEnPcbs(self,limiteDelBloque,bdDelBloqueSiguiente,sizeDelBloque):
		bloque = Bloque(limiteDelBloque + 1,bdDelBloqueSiguiente -1)
		index = limiteDelBloque + 1
		pcb = self._pcbTable.getPcbXBd(limiteDelBloque +1)
		#print(index - sizeDelBloque)
		pcb.setBd(index - sizeDelBloque)
		for i in range(bloque.getBd(), bloque.getLimite()):
			if self._memory.getIndexMemory(i).isExit():
				if i != bloque.getLimite():
					pcb1 = self._pcbTable.getPcbXBd(i +1 )
					pcb1.setBd(i +1 - sizeDelBloque)

					
	def actualizarMemoria(self,limiteDelBloque,bdDelBloqueSiguiente,sizeDelBloque):

		## modificar el MMu y los pcbs
		#memory = self._memory.getMemory() 
		#rec = sizeDelBloque #(bdDelBloqueSiguiente) - (limiteDelBloque +1)
		#rec = (bdDelBloqueSiguiente-(limiteDelBloque + 1))-1
		index = limiteDelBloque +1 
		rec = (bdDelBloqueSiguiente-index)
		#pcb = self._pcbTable.getPcbXBd(limiteDelBloque +1)
		#print(index - sizeDelBloque)
		#print(rec)
		#print(index)
		#print(bdDelBloqueSiguiente)
		#print(sizeDelBloque)
		#pcb.setBd(index - sizeDelBloque)
		self.desarmarBloqueEnPcbs(limiteDelBloque,bdDelBloqueSiguiente,sizeDelBloque)

		while(rec != 0):
			#instructions = self._memory.getMemory()[index]
			instructions = self._memory.getIndexMemory(index)
			#print "Inst: {0}: {1}".format(index,instructions)
			self._memory.setIndexMemory(index - sizeDelBloque, instructions)
			self._memory.setIndexMemory(index , None)
			#self._memory.setIndexMemory(index - rec, instructions)
			#print(index)
			index = index +1
			
			rec = rec -1
		#print memory

		#self.clearAsigContinua(Bloque(limiteDelBloque +1, bdDelBloqueSiguiente -1))


	def clear(self, pcb):
		self._memoryManager.liberarMemoria(pcb)	
		
	def clearMemory(self, bloque):
		bd = bloque.getBd()
		#print(bd)
		limite = bloque.getLimite()+1
		for i in range (bd, limite):

			#self._memory.getMemory()[i]  = None	
			self._memory.setIndexMemory(i,None)
	def setMmu(self, mmu):
		self._mmu = mmu

	def loadPag(self,program, listaDePaginas, pcb):
		p = program.instructions#self._disk.getProgram(pcb.getNameProgram()).instructions
		pc = pcb.getPc()
		for i in listaDePaginas:
			for x in range(i.getBd(), i.getLimite() + 1):
				if(pc < len(p)):
					self._memory.setIndexMemory(x, p[pc])
					pc +=1
				else:
					break

	def getDisk(self):
		return self._disk