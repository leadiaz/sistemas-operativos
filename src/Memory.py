class Memory():

    def __init__(self, tamanho):
        self._tamanho = tamanho
        self._memory = self.crearMemoria(tamanho)

    def crearMemoria(self, tamanho):
        memory = []
        for i in range ( tamanho):
            memory.append(None)
        return memory
    def load(self, program):
        self._memory = self._memory + program.instructions

    def fetch(self, addr):
        print (self._memory[addr])
        return self._memory[addr]

    def getMemory(self):
        return self._memory
    def getTamanho(self):
        return self._tamanho    
    def size(self):
        return len(self._memory)

    def getIndexMemory(self, index):
        return self._memory[index]
    def setIndexMemory(self, index, instructions):
        #self._memory.insert(index, instructions)             
        self._memory[index]=instructions


    def __repr__(self):
        ret = '' 
        count = 0 #solo agregado para omitir tabulate
        for i in self._memory:
           print ('| ' + str(count) +' | ' + str(i) + '|')
           count +=1           
        return ret
        #return tabulate(enumerate(self._memory), tablefmt='psql')