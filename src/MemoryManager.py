from Bloque import *
from BuscarBloque import *
from Memory import *
from SelectVictim import *
class MemoryManager(object):
	def __init__(self,buscador, memory, bloque, tamanho, loader):
		self._bloqueInicialLibre = bloque
		self._free = tamanho
		self._buscarBloque = buscador
		self._memory = memory
		self._loader = loader
		self._espaciosLibres = 0;		

	def getFree (self):
		return self._free
	def setFree (self, tamanho):
		self._free = tamanho
			
	def setBloqueInicial(self, bloque):
		self._bloqueInicialLibre = bloque
	def setMemory(self, memo):
		self._memory = memo


class AsignacionContinua(MemoryManager):


	def getBloqueInicial(self):
		return 	self._bloqueInicialLibre 
	def liberarMemoria(self,pcb):
		self._free = self._free + pcb.size()
		bloque = Bloque(pcb.getBd(), (pcb.getBd() + pcb.size())-1)
		#bloque.setNext(self._bloqueInicialLibre)
		#self._bloqueInicialLibre = bloque
		self.actualizarListaDEBloque(bloque, None,self._bloqueInicialLibre)
		self._loader.clearMemory(bloque)
		## Falta compactar los bloques, osea si hay un bloque con limte 25 y el siguiente bloque tiene el bd = 26
		## lo ideal seria crear un bloque que conjunte a esos dos bloque anteriormente descriptos 
		self.compactarLosBloques(None,self.getBloqueInicial())

	def load(self,programa, pcb):
		if self._free >= programa.getSize():
			bloque = self._buscarBloque.getBloque(self._bloqueInicialLibre, programa.getSize())
			if bloque == None:## el buscador no encontro un bloque con la suficiente capacidad pero el espacio free alcanza
				self.compactar(self._bloqueInicialLibre) ## se larga el compactar
				## al momento de compactar el SO se tiene que parar
				## se puede hacer con interrupcion en el iterrupManager
				## self._interrumpManager.stopMachine()
				bloque = self._buscarBloque.getBloque(self._bloqueInicialLibre, programa.getSize())

			bdDelBloque = bloque.getBd()	
			nuevoBloqueLibre =self.evaluarBloque(bloque,programa)## retorna el bloque si al  evaluar el bloque que fue asignado
			#print(nuevoBloqueLibre.getBd())
			#print(bdDelBloque)
			#print(nuevoBloqueLibre.getBd())
			if nuevoBloqueLibre.getBd() == bdDelBloque:
				self.sacarBloque(bloque, None, self._bloqueInicialLibre)
			#else: 
			#	print(self._bloqueInicialLibre.getBd())
				##self.actualizarListaDEBloque(nuevoBloqueLibre, None,self._bloqueInicialLibre )	


			self._loader.loadAsigConti(bdDelBloque, programa, pcb)
			self._free = self._free - programa.getSize()
			##self._bloqueInicialLibre = bloque #Bloque(programa.size()-1 ,255) 


	def evaluarBloque(self, bloque, programa):
		margen = (bloque.getLimite() - bloque.getBd()) - (programa.getSize()-1)
		if  margen == 0:
			return bloque ## El programa abarca en su totalidad el bloque que se le asigno 
		else:
			bd = bloque.getBd()
			#print(bd)
			#print(bd + programa.getSize())
			bloque.setBd(bd + programa.getSize())
			return bloque				

	def compactarLosBloques(self, bloqueAnterior, bloqueDEPartida):
		if bloqueDEPartida == None :
			return True

		if bloqueDEPartida.nextBloque() == None :
			return False

		if bloqueDEPartida.size() == self._free:
			return True  		

		if bloqueAnterior == None:
			if bloqueDEPartida.getLimite() + 1 == bloqueDEPartida.nextBloque().getBd():
				bloqueDEPartida.setLimite(bloqueDEPartida.nextBloque().getLimite())
				bloqueDEPartida.setNextBloque(bloqueDEPartida.nextBloque().nextBloque())
				self._bloqueInicialLibre = bloqueDEPartida
				if bloqueDEPartida.nextBloque() == None :
					return False
		if bloqueDEPartida.getLimite() + 1 == bloqueDEPartida.nextBloque().getBd():
			bloqueDEPartida.setLimite(bloqueDEPartida.nextBloque().getLimite())
			bloqueDEPartida.setNextBloque(bloqueDEPartida.nextBloque().nextBloque())	 
		self.compactarLosBloques(bloqueDEPartida,bloqueDEPartida.nextBloque())	
				

	def compactar(self, bloqueDEPartida):
		self._espaciosLibres = self._espaciosLibres + bloqueDEPartida.size()
		if bloqueDEPartida.nextBloque() == None:## verifica si es el ultimo bloque enlazado
			i = (self._memory.getTamanho() -1)   - bloqueDEPartida.getLimite()
			if i  >= 1: ## pregunta si queda alguna instruccion cargada en la memoria, si es asi actualiza ese proceso en la memoria 
				self._loader.actualizarMemoria(bloqueDEPartida.getLimite(),(self._memory.getTamanho() -1) ,bloqueDEPartida.size())

			#bloque = Bloque( self._free  - self._espaciosLibres, self._free -1 )
			bloque = Bloque( (self._memory.getTamanho() -1)  - (self._espaciosLibres ), self._memory.getTamanho() -1 )
			self._bloqueInicialLibre = bloque ## crea el nuevo bloque compactado y lo asigno como el principal
			#print(self._espaciosLibres)
			#print(bloque.getBd())
			self._espaciosLibres = 0;
			return True 
		else:
			self._loader.actualizarMemoria(bloqueDEPartida.getLimite(),bloqueDEPartida.nextBloque().getBd(),bloqueDEPartida.size()+1)
			self.compactar(bloqueDEPartida.nextBloque()) ## genero la recursion si aun no es el ultimo bloque
 


	def actualizarListaDEBloque(self, bloqueAPoner, bloqueAnterior, bloqueDEPartida):
		if bloqueAnterior == None:
			if bloqueAPoner.getBd() < bloqueDEPartida.getBd():
				bloqueAPoner.setNextBloque(bloqueDEPartida)
				self._bloqueInicialLibre = bloqueAPoner
				return True
		if bloqueDEPartida.nextBloque() == None:
			if bloqueAPoner.getBd() > bloqueDEPartida.getBd():
				bloqueDEPartida.setNextBloque(bloqueAPoner)
				bloqueAPoner.setNextBloque(None)
				return True
			elif bloqueAPoner.getBd() < bloqueDEPartida.getBd():
				bloqueAPoner.setNextBloque(bloqueDEPartida)
				bloqueAnterior.setNextBloque(bloqueAPoner)	
				return True
			return False	
			 ## significa que es el ultimo bloque 
		if bloqueAPoner.getBd() > bloqueDEPartida.getBd():## recorre recursivamente hasta que encuentra un lugar donde poner el bloque	
			self.actualizarListaDEBloque(bloqueAPoner,bloqueDEPartida, bloqueDEPartida.nextBloque()) 
		else:
			##return self.getBloque(bloque.next(), size)
			bloqueAPoner.setNextBloque(bloqueAnterior.nextBloque())
			bloqueAnterior.setNextBloque(bloqueAPoner)
			return True
			## casos de cuando es el ultimo o el iniacl del bloque 
	def sacarBloque(self,bloqueASacar, bloqueAnterior, bloqueDEPartida):
		## SOLO ANDA CON MAS DE DOS BLOQUES ENLASADOS
		if bloqueAnterior == None :
			if bloqueASacar.getBd() == bloqueDEPartida.getBd():
				self._bloqueInicialLibre= bloqueDEPartida.nextBloque()
				return True
		if bloqueDEPartida.nextBloque() == None: 
			return True
		if bloqueASacar.getBd() > bloqueDEPartida.getBd():## recorre recursivamente hasta que encuentra el bloque a sacar
			self.sacarBloque(bloqueASacar,bloqueDEPartida, bloqueDEPartida.nextBloque())
		else: 
			bloqueAnterior.setNextBloque(bloqueDEPartida.nextBloque())
			return  True

	def puedeCargar(self, program):
		return  program.size <= self._free
      
class Paginacion(MemoryManager):

    def __init__(self, tamanhoDePaginas, memory, loader, pcbtable):
        self._memory = memory
        self._tamanhoDePaginas = tamanhoDePaginas
        self._paginas = self.genenrarPaginas(tamanhoDePaginas)
        self._loader = loader
        self._tablaDePcb = pcbtable
        self._paginasLibres = len(self._paginas)


    def getPaginas(self):
        return self._paginas
    def tamanhoDePaginas(self):
        return self._tamanhoDePaginas


    def genenrarPaginas(self, tamanhoDePagina):
        paginas= []
        cantidadDePaginas = self._memory.size()/tamanhoDePagina
        if(self._memory.size()%tamanhoDePagina > 0):
            cantidadDePaginas +=1
        bd = 0
        limite = tamanhoDePagina - 1
        for i in range(int(cantidadDePaginas)): ## puse int(cantidadDePaginas) para que en mi pc ande
            b = Bloque(bd, limite)				## porque tiraba un error de float no se que

            bd += 4								## cualquier cosa sacalo y dejalo como estaba antes: cantidadDePaginas
            limite += 4							 
            paginas.append(b)

        return paginas

    def calcularPaginas(self, program):
        cantPaginas = program.size / self._tamanhoDePaginas
        if program.size % self._tamanhoDePaginas > 0:
            cantPaginas += 1
        return cantPaginas

    def load(self, program, pcb):

        listaDePag = self.buscarMarcos(self.calcularPaginas(program))
        self._loader.loadPag(program, listaDePag,pcb)
        #pcb.setTamanhoDeMarco(self._tamanhoDePaginas)
        pcb.setTablaDePaginas(listaDePag)


    def buscarMarcos(self, cantidad):
        x = []
        cant = 0
        for i in self._paginas:
            if not i.estaOcupado() and len(x) < cantidad:
                i.setEstaOcupado(True)
                x.append(i)
                cant += 1
        self._paginasLibres -= cant
        return x

    def puedeCargar(self, program):
        return self.calcularPaginas(program)<= self._paginasLibres

    def liberarMemoria(self, pcb):
        for i in pcb.getTablaDePagina():
            self._loader.clearMemory(i)

    def getPaginasLibres(self):
    	return self._paginasLibres

class PaginacionBajoDemanda(Paginacion):
	def __init__(self, tamanhoDePaginas, memory, loader, pcbtable, vicselector):
		super(PaginacionBajoDemanda, self).__init__(tamanhoDePaginas, memory, loader, pcbtable)
		self._vic = vicselector

	def puedeCargar(self, program):
		return True

	def buscarMarcos(self, cantidad):
		for i in self._paginas:
			if not i.bit():
				aux = i
				i.setBit(True)
				return aux

	def load(self, program, pcb):
		pag = self.buscarMarcos(1)
		pcb.setNameProgram(program.name)
		if pag == None:
			self._vic.seleccionarvictimaSecondChance(self._paginas)
			pag = self.buscarMarcos(1)
		self._loader.loadPag(program, [pag], pcb)
		pcb.setTablaDePaginas([pag])

	def loadPageFault(self, pcb):
		program = self._loader.getDisk().getProgram(pcb.getNameProgram())
		self.load(program, pcb)