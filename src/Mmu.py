class Mmu(object):
    def __init__(self, memory):
        self._bd = 0
        self._memory = memory
        self._pcbActual = None

    def fetch(self, dirLogica):
        # devuelve  lo que hay  en la direccion fisica  de la  memoria
        dirFisica = self._bd + dirLogica
        dato = self._memory.fetch(dirFisica)
        return dato

    def setDB(self, dirbase):
        self._bd = dirbase

    def getBd(self):
        return self._bd
    def setMemory(self,memo):
        self._memory = memo    
    def setPcb(self, pcb):
        self._pcbActual = pcb

class MMUPaginacion(Mmu):
    def __init__(self, memory, tablaDePaginas, tamanhoDePaginas):
        super(MMUPaginacion, self).__init__(memory)
        self._tablaDePaginas = tablaDePaginas
        self._tamanhoDePaginas = tamanhoDePaginas
        self._interruptManager = None

    def fetch(self, dirLogica):
        nroPagina = dirLogica / self._tamanhoDePaginas
        desplazamiento = dirLogica % self._tamanhoDePaginas
        pagina = self._pcbActual.getTablaDePagina()[nroPagina]
        print (pagina)
        return self._memory.fetch(pagina.getBd() + desplazamiento)
    def setInterruptManager(self, interrupt):
        self._interruptManager = interrupt
    def setTablaDePaginas(self,pag):
        self._tablaDePaginas = pag
    def setTamanhoDePagina(self, n):
        self._tamanhoDePaginas = n

class MMUPaginacionBajoDemanda(MMUPaginacion):
    def fetch(self, dirLogica):
        nropag = dirLogica / self._tamanhoDePaginas
        despl = dirLogica % self._tamanhoDePaginas
        pag = self._pcbActual.getTablaDePagina()
        if pag[0].getBd() == self._tablaDePaginas[int(nropag)].getBd() :
            return self._memory.fetch(pag[0].getBd() + despl)
        else:
            self._interruptManager.pageFault()
            pag = self._pcbActual.getTablaDePagina()
            return  self._memory.fetch(pag[0].getBd() + despl)
