class Pcb():

	def __init__(self, id, dirbase, prioridad, size):
		self._id = id
		self._dirBase = dirbase
		self._pc = 0
		self._state = "NEW"
		self._prioridad = prioridad
		self._size = size
		self._tablaDePaginas = []
		self._tiempoEspera = 0
		self._nameProgram = ""

	def getTiempoEspera(self):
		return self._tiempoEspera
			
	def sumar1TiempoEspera(self):
		self._tiempoEspera = self._tiempoEspera +1	
		

	def id(self):
		return self._id

	def prioridad(self):
		return self._prioridad

	def size(self):
		return self._size

	def setPc(self, pc):
		self._pc = pc
	def getPc(self):
		return self._pc

	def getState(self):
		return self._state
	def getBd(self):
		return self._dirBase		

	def setState(self, state):
		self._state = state
		
	def setSize(self,size):
		self._size = size

	def setBd(self, bd):
		self._dirBase = bd
	def setId(self, id):
		self._id = id
	def getTablaDePagina(self):
		return self._tablaDePaginas

	def setTablaDePaginas(self, tablaDePaginas):
		self._tablaDePaginas = tablaDePaginas
	def setNameProgram(self, name):
		self._nameProgram = name
	def getNameProgram(self):
		return self._nameProgram

	def __repr__(self):
		return "Pcb(DirBase={pc}), paginas = {pag}".format(pc=self._dirBase, pag=self._tablaDePaginas)