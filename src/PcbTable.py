class PcbTable():

	def __init__(self):
		self._pcbs= []


	def add(self, pcb):
		##self._pcbs + pcb
		pcb.setId(self.nextID())
		self._pcbs.append(pcb)

	def modificarPcb(self,pcb):
		self._pcbs[pcb.id()] = pcb  
	def nextID(self):
		return len(self._pcbs)
	def getPcb(self,ids):
		return	self._pcbs[ids]
	def getPcbXBd(self,bd):
		aux =0;
		while aux !=len(self._pcbs):
			if self._pcbs[aux].getBd() == bd:
				if self._pcbs[aux].getState() != "TERMINATED":
					return self._pcbs[aux]
			else:
				aux = aux +1
		return None			
	def tiempoEsperaPromedio(self):
		aux = 0
		for i in self._pcbs : 
			aux = aux + i.getTiempoEspera()

		return  aux/ len(self._pcbs)		
		