from Instruccion import *

from Scheduler import *
class Program():

    def __init__(self, name, instructions):
        self._name = name
        self._instructions = self.expand(instructions)

    @property
    def name(self):
        return self._name

    @property
    def instructions(self):
        return self._instructions

    def expand(self, instructions):
        expanded = []

        for instr in instructions:
            expanded.extend(instr.expand())

        if not expanded[-1].isExit():
            expanded.append(EXIT(0))

        return expanded

    @property
    def size(self):
        size = len(self._instructions)
        return size
    def getSize(self):
        size = len(self._instructions)
        return size   

    def __repr__(self):
        return "Program({name}, {instructions})".format(name=self._name, instructions=self._instructions)