
class Scheduler(object):
	def __init__(self):
		self._colaReady =[]
		self._esPaginacion = False



	def setEsPaginacion(self, boole):
		self._esPaginacion = boole	
	def getNext(self):
		pass

	def add(self,pcb):
		pass
		

	'''def remove(self, pcb):
		self._procesos.remove(pcb.id)
		'''
	def colaReady(self):
		return self._colaReady

	def tick(self):
		for i in self._colaReady : 
			i.sumar1TiempoEspera()
		#print("tick:" + str(1))	

	def hasNext(self):
		return len(self._colaReady)>0

	def esExpropiativo(self):
		return False



class FCFS(Scheduler):
	def add(self,pcb):
		
		self._colaReady.append(pcb)

	def getNext(self):

		if (self._colaReady != []):
			return self._colaReady.pop(0)


class Prioridad(Scheduler):


	def add(self,pcb):
		
		self._colaReady.append(pcb)
		self._colaReady = sorted(self._colaReady, key=lambda pcb: pcb.prioridad())
		#self.quicksort(self._colaReady, 0, (len(self._colaReady)-1))
		self._colaReady.reverse()


	def getNext(self):
		if (self._colaReady != []):
			return self._colaReady.pop(0)

class PrioridadExpropiativo(Prioridad):

	def esExpropiativo(self):
		return True

	def compararOrden(self, pcb1, pcb2):
		if pcb1.prioridad() > pcb2.prioridad():
			return pcb1
		else:
			return pcb2


class SJF(Scheduler):

	def add(self, pcb):
		self._colaReady.append(pcb)
		self._colaReady = sorted(self._colaReady, key=lambda pcb: pcb.size())

	def getNext(self):
		if (self._colaReady != []):
			return self._colaReady.pop(0)


class SJFExpropiativo(SJF):

	def esExpropiativo(self):
		return True

	def compararOrden(self, pcb1, pcb2):
		if pcb1.size() < pcb2.size():
			return pcb1
		else :
			return pcb2
#class RoundRobin(FCFS):
class RoundRobin(FCFS):
	"""docstring for ClassName"""
	def __init__(self, quamtun):
		super(RoundRobin, self).__init__()
		self.quamtun = quamtun
		self._contador = 0
		#self._colaReady = []
		self._interruptManager= None

	def setInterrupt(self, inte):
		self._interruptManager = inte

	def tick(self):
		if self._esPaginacion: 
			if self._interruptManager.getDispatcher().getCpu().estaEnUso():
				self._contador +=1
			#print("contador:" +str(self._contador))
				for i in self._colaReady :
					i.sumar1TiempoEspera()
			#print("ticks:")

				if self.quamtun == self._contador:
					self._interruptManager.timeOut()
					self._contador = 0
		#else:
			#self._contador = self._contador + 1
		else:
			self._contador +=1
			#print("contador:" +str(self._contador))
			for i in self._colaReady :
				i.sumar1TiempoEspera()
			#print("ticks:")

			if self.quamtun == self._contador:
				self._interruptManager.timeOut()
				self._contador = 0
					
	def tickParaTestear(self):
		self._contador +=1
		if self.quamtun == self._contador:
			self.getNext()
			self._contador = 0					
		


