import unittest
from BuscarBloque import *
from Bloque import *
class TestBestFit(unittest.TestCase):


    def setUp(self):
        self._buscador = BestFit()
        self._bloque1  = Bloque(0, 12)
        self._bloque2 = Bloque(23, 52)
        self._bloque3 = Bloque(60, 70)

        self._bloque1.setNextBloque(self._bloque2)
        self._bloque2.setNextBloque(self._bloque3)

    def testGetBloque(self):
        print (self._bloque1.size())
        print (self._bloque2.size())
        print (self._bloque3.size())
        miBloque = self._buscador.getBloque(self._bloque1, 12)
        miBloque2 = self._buscador.getBloque(self._bloque1, 10)
        miBloque3 = self._buscador.getBloque(self._bloque1, 28)
        print (miBloque.size())
        self.assertEqual(miBloque, self._bloque1)
        self.assertEqual(miBloque.size(), 12)
        self.assertEqual(miBloque2, self._bloque3)
        self.assertEqual(miBloque2.size(), 10)
        self.assertEqual(miBloque3, self._bloque2)
        self.assertEqual(miBloque3.size(), 29)


if __name__ == '__main__':
    unittest.main()