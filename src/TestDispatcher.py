import unittest
from Dispatcher import *
from Mmu import *
from Memory import *
from Pcb import *
from Cpu import *
class TestDispatcher(unittest.TestCase):
	def setUp(self):
		self._dispatcher = Dispatcher()	
		self._memory = Memory(100)
		self._mmu = Mmu(self._memory)
		self._cpu = Cpu()
		self._dispatcher.setCpu(self._cpu)
		self._dispatcher.setMmu(self._mmu)
		self.p = Pcb(0,0,9,7)
		self.p.setPc(2)
		self.p1 = Pcb (1,5, 8,10)
		self.p2 = Pcb (2,8, 2,4)

	def testloadDispatcher(self):
		self.assertEqual(self._cpu.pc, 0)
		self.assertEqual(self._mmu.getBd(), 0)
		self._dispatcher.load(self.p)
		self.assertEqual(self._cpu.pc, 2)
		self.assertEqual(self._mmu.getBd(), 0)
	def testSave(self):
		self._dispatcher.load(self.p)
		self.assertEqual(self.p.getPc(), 2)
		self._cpu.pc = 4
		self._dispatcher.save()
		self.assertEqual(self.p.getPc(), 4)


if __name__ == '__main__':
	unittest.main()
