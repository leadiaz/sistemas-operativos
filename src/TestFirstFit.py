import unittest
from BuscarBloque import *
from Bloque import *
class TestFirsFit(unittest.TestCase):


    def setUp(self):
        self._buscador = FirstFit()
        self._bloque1  = Bloque(0, 12)
        self._bloque2 = Bloque(23, 52)
        self._bloque3 = Bloque(60, 70)

        self._bloque1.setNextBloque(self._bloque2)

    def testGetBloque(self):


        self._bloque2.setNextBloque(self._bloque3)
        self.assertTrue(self._bloque1.hasNext())
        miBloque = self._buscador.getBloque(self._bloque1, 15)
        self.assertEqual(miBloque, self._bloque2)
        self.assertEqual(miBloque.size(), 29)
        
if __name__ == '__main__':
    unittest.main()