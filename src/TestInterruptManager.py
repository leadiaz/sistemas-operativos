import unittest
from Dispatcher import *
from Mmu import *
from Memory import *
from Pcb import *
from PcbTable import *
from Loader import *
from InterruptManager import *
from Cpu import *
from Instruccion import *
from Programa import *
from MemoryManager import *
class TestInterrumpManager(unittest.TestCase):
	def setUp(self):	
		self._dispatcher = Dispatcher()
		self._schedule = FCFS()
		self._scheduleExpropiativo = SJFExpropiativo()
		self._cpu = Cpu()
		self._pcbTable = PcbTable()
		self._ioDevice = IODevice()	
		self._memory = Memory(100)
		self._mmu = Mmu(self._memory)
		self._disk = Disk()
		self._loader =  Loader(self._memory,self._disk,self._mmu)
		self._buscador = FirstFit()
		self._bloqueInicial = Bloque(0,99)
		self._memoryManager = AsignacionContinua(self._buscador, self._memory, self._bloqueInicial, 100, self._loader)
		self._loader.setMemoryManager(self._memoryManager)
		self._loader.setPcbTable(self._pcbTable)
		self._interruptManager =InterruptManager(self._pcbTable, self._loader)
		self._dispatcher.setMmu(self._mmu)
		self._dispatcher.setCpu(self._cpu)
		self._interruptManager.setScheduler(self._schedule)
		self._interruptManager.setCpu(self._cpu)
		self._interruptManager.pcbTable = self._pcbTable
		self._interruptManager.setDispatcher(self._dispatcher)
		self._program1 = Program("programaTest", [CPU(3),IO(2)])
		self._program2 = Program("programaTest2", [CPU(3),IO(1)])
		self._program3 = Program("programaTest3", [CPU(3),IO(3)])
		self.p = Pcb(0,0,9,25)
		self.p1 = Pcb (1,5, 8,56)
		self._interruptManager.setIoDevice(self._ioDevice)
		self._interruptManager.setMemorymanager(self._memoryManager)

	def testNewConCpuSinUso(self):
		self._disk.addDatos(self._program1)
		self.assertEqual(self._cpu.estaEnUso(), False)
		self.assertEqual(self._mmu.getBd(),0)
		self._interruptManager.new(self._program1,5)
		self.assertTrue(self._cpu.estaEnUso())
		self.assertEqual(self._mmu.getBd(),0)
		self.assertEqual(self._cpu.pc, 0)
		self.assertEqual(self._dispatcher.pcbActual().prioridad(),5)
		self.assertEqual(self._dispatcher.pcbActual().getState(), "RUNNING")
		##self._dispatcher.load(self.p)
		##self.assertTrue(self._cpu.estaEnUso())
	def testNewConCpuEnUso(self):
		## al estar el cpu en uso y el scheduler siendo No Expropiativo
		#$ el pcb del program1 tiene que ir a la cola De Ready del scheduler 
		self._disk.addDatos(self._program1)
		self._cpu.setEstaEnUso(True)
		self.assertEqual(self._cpu.estaEnUso(), True)
		self.assertEqual(len(self._schedule.colaReady()),0)
		self._interruptManager.new(self._program1,5)
		self.assertEqual(len(self._schedule.colaReady()),1)
	def testNewConCpuEnUsoYSchedulerExpropiativo(self):
		## al estar el cpu en uso y el scheduler siendo  Expropiativo
		##el  program2 al tener la rafaga mas corta entra directo al cpu por medio del dispatcher
		## y el pcb del program1 se actualiza y va a la cola de ready del scheduler
		self._interruptManager.setScheduler(self._scheduleExpropiativo)
		self._disk.addDatos(self._program1)
		self._disk.addDatos(self._program2)
		self._disk.addDatos(self._program3)
		self._interruptManager.new(self._program1,5)
		self.assertEqual(self._cpu.estaEnUso(), True)
		self.assertEqual(self._dispatcher.pcbActual().size(), 6)
		self.assertEqual(self._dispatcher.pcbActual().prioridad(), 5)
		self.assertEqual(len(self._scheduleExpropiativo.colaReady()),0)
		self._interruptManager.new(self._program2,6)
		self.assertEqual(self._dispatcher.pcbActual().size(), 5)
		self.assertEqual(self._dispatcher.pcbActual().prioridad(), 6)
		self.assertEqual(len(self._scheduleExpropiativo.colaReady()),1)
		## el actual proceso en ejecucion es el programa2 el cual le saco el cpu al programa1 por tener la rafaga mas corta
		self._interruptManager.new(self._program3,4)
		self.assertEqual(self._dispatcher.pcbActual().size(), 5)
		self.assertEqual(self._dispatcher.pcbActual().prioridad(), 6)
		self.assertEqual(len(self._scheduleExpropiativo.colaReady()),2)
		## al agregar un nuevo programa cuya rafaga es mayor que la actual directamente lo agrega a la cola de ready del scheduler

	def testIo(self):
		self._disk.addDatos(self._program1)
		self._disk.addDatos(self._program2)
		##self._dispatcher.load(self.p)
		self._interruptManager.new(self._program1,5)
		self.assertEqual(self._mmu.getBd(),0)
		self._interruptManager.new(self._program2,6)
		self.assertEqual(self._mmu.getBd(),0)
		self.assertEqual(self._dispatcher.pcbActual().id(),0)
		##self._schedule.add(self.p1)
		self.assertEqual(len(self._ioDevice.queueWaiting()),0)
		self._interruptManager.io()	
		## al recibir la interrupcion de IO coloca el pcbactual(program1) de id 0 en la cola de waiting del IoDevice
		## como el scheduler tiene un pcb(program2)de id 1  en la cola de ready, este es cargado via el dispatcher en el cpu 
		self.assertEqual(len(self._ioDevice.queueWaiting()),1)
		self.assertEqual(self._mmu.getBd(),6)
		self.assertEqual(self._dispatcher.pcbActual().id(),1)

	def testTimeOut(self):
		self._dispatcher.load(self.p)
		self._schedule.add(self.p1)
		self._cpu.setPc(1)
		self.assertEqual(self._mmu.getBd(),0)
		self.assertEqual(self._dispatcher.pcbActual().id(),0)
		self.assertEqual(self._cpu.getPc(),1)
		self._interruptManager.timeOut()
		## al recibir la interrupcion de timeOut el pcb actual (p) id = 0 es colocado en la cola de ready el scheduler
		## el pcb(p1)id=1  que estaba en la colda de ready es cargado por el dispatcher en el cpu 
		self.assertEqual(self._dispatcher.pcbActual().id(),1)
		self.assertEqual(len(self._schedule.colaReady()),1)
		self.assertEqual(self._schedule.colaReady()[0].getPc(),1)#El pc del pcb de p1 es 1 ya que se guardo con ese digito
		self.assertEqual(self._cpu.getPc(),0)

	def testKill(self):
		self._disk.addDatos(self._program1)
		self._disk.addDatos(self._program2)
		self._interruptManager.new(self._program1,5)
		self._interruptManager.new(self._program2,6)
		self.assertEqual(len(self._schedule.colaReady()),1)
		self.assertEqual(self._pcbTable.getPcb(0).getState(),"RUNNING")## indica que el pcb del program1(id=0) esta en Running
		self.assertEqual(self._pcbTable.getPcb(1).getState(),"READY")## este proceso esta ne la colda de ready
		self._interruptManager.kill()
		## Al activarse esta interrupcion el pcbactual(program1) id = 0 se indica como terminado en la pcbtable
		## y se empieza a ejercutar el proximo pcb que se encuentra en la cola de ready del scheduler
		## en este caso es el pcb del program2, id = 1
		self.assertEqual(self._pcbTable.getPcb(0).getState(),"Terminated")
		self.assertEqual(self._dispatcher.pcbActual().id(),1)	
		self.assertEqual(self._pcbTable.getPcb(1).getState(),"RUNNING")	
	def testIOComplete(self):
		self._dispatcher.load(self.p)
		self.assertEqual(self._mmu.getBd(),0)
		self.assertEqual(self._dispatcher.pcbActual().id(),0)
		self.assertEqual(len(self._schedule.colaReady()),0)
		self._interruptManager.completeIO(self.p1)
		self.assertEqual(len(self._schedule.colaReady()),1)
	def testIOCompleteConSchedulerExpropiativo(self):
		self._interruptManager.setScheduler(self._scheduleExpropiativo)
		self._dispatcher.load(self.p1)
		self.assertEqual(self.p1.getBd(),5)
		self.assertEqual(self._mmu.getBd(),5)
		self.assertEqual(self._dispatcher.pcbActual().id(),1)
		self.assertEqual(len(self._scheduleExpropiativo.colaReady()),0)
		self._interruptManager.completeIO(self.p)
		self.assertEqual(self._mmu.getBd(),0)
		## Al ser el pcb (p, id = 0) el pcb con la rafaga mas corta (size = 25) es cargado directamente al cpu por el dispatcher
		## y el pcb(p1, id = 1) con la rafa mas largar(size = 56) es llevado a la cola de ready del scheduler
		self.assertEqual(len(self._scheduleExpropiativo.colaReady()),1)
		self.assertEqual(self._dispatcher.pcbActual().id(),0)	


if __name__ == '__main__':
	unittest.main()
	
