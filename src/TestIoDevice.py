from IODevice import *
from Pcb import *
from InterruptManager import *
from Scheduler import *
from PcbTable import *
from Loader import *
import unittest
class TestIoDevice(unittest.TestCase):
	def setUp(self):
		self._ioDevice = IODevice()
		self._pcbTable = PcbTable()
		self._memory = Memory(100)
		self._mmu = Mmu(self._memory)
		self._disk = Disk()
		self._loader = Loader(self._memory,self._disk,self._mmu)
		self._interrumpManager = InterruptManager(self._pcbTable,self._loader)
		self.p = Pcb(0,0,9,25)
		self.p.setPc(2)
		self.p1 = Pcb (1,5, 8,56)
		self.p2 = Pcb (2,8, 2,10)
		self._scheduler = FCFS()
		self._interrumpManager.IoDevice= self._ioDevice
		self._interrumpManager.setScheduler(self._scheduler)
		self._ioDevice.setInterrumpManager(self._interrumpManager)
	
	def testloadNextQueueWaiting(self):
		self._ioDevice.loadNextQueueWaiting()
		self.assertEqual(len(self._ioDevice.queueWaiting()), 0)
		self._ioDevice.add(self.p)
		self.assertEqual(len(self._ioDevice.queueWaiting()), 1)
		self._ioDevice.loadNextQueueWaiting()
		## Verifico que si hay algun pcb en la queueWaitng el metodo de arriba los coloca como currentPCB y lo saca de la
		## queue de Waiting
		self.assertEqual(len(self._ioDevice.queueWaiting()), 0)
		self.assertEqual(self._ioDevice.getPCB().id(),0)

	def testkeepRunning(self):
		self._ioDevice.add(self.p)
		self.assertEqual(len(self._ioDevice.queueWaiting()), 1)
		self.assertEqual(self._ioDevice.getTickCount(), 0)
		self._ioDevice.loadNextQueueWaiting()
		self._ioDevice.keepRunning()
		self.assertEqual(len(self._ioDevice.queueWaiting()), 0)
		self._ioDevice.keepRunning()
		self.assertEqual(self._ioDevice.getTickCount(), 3)
		self._ioDevice.keepRunning()
		## verifico si el pcb cambia su estado al finalizar su IO 
		## Ademas de si dicho pcb se agrego al scheduler 
		self.assertEqual(self.p.getState(), "READY")
		self.assertEqual(self._scheduler.getNext().id(), 0)

	def testTick(self):
		self._ioDevice.add(self.p)
		self.assertEqual(len(self._ioDevice.queueWaiting()), 1)	
		self.assertEqual(self._ioDevice.getTickCount(), 0)
		self._ioDevice.tick()
		self.assertEqual(len(self._ioDevice.queueWaiting()), 0)
		self.assertEqual(self._ioDevice.getPCB().id(), 0)
		self._ioDevice.tick()
		self.assertEqual(self._ioDevice.getTickCount(), 2)
		self._ioDevice.tick()
		self.assertEqual(self._ioDevice.getTickCount(), 3)
		self._ioDevice.tick()
		## verifico si el pcb cambia su estado al finalizar su IO 
		## Ademas de si dicho pcb se agrego al scheduler
		self.assertEqual(self.p.getState(), "READY")
		self.assertEqual(self._scheduler.getNext().id(), 0)

if __name__ == '__main__':
	unittest.main()			