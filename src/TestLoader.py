import unittest
from Mmu import *
from Memory import *
from Loader import *
from Programa import *
from Disk import *
from PcbTable import *
from Pcb import *
from BuscarBloque import *
from MemoryManager import *
class TestLoader(unittest.TestCase):
	def setUp(self):
		self._memory = Memory(30)
		self._mmu = Mmu(self._memory)
		self._pcbTable = PcbTable()
		self._program1 = Program("programaTest", [CPU(3),IO(2)])
		self._p1 = Pcb(0,0,9,6)
		self._pcbTable.add(self._p1)
		self._disk = Disk()
		self._disk.addDatos(self._program1)
		self._loader =  Loader(self._memory,self._disk,self._mmu)

	def testLoaderAsignacionContinua(self):
		buscador = FirstFit()
		bloqueInicial = Bloque(0,29)
		memoryManager = AsignacionContinua(buscador, self._memory, bloqueInicial, 30, self._loader)
		self._loader.setMemoryManager(memoryManager)
		self._loader.setPcbTable(self._pcbTable)
		self.assertEqual(self._mmu.getBd(),0)
		self._loader.load(self._program1, self._p1)
		self.assertEqual(self._mmu.getBd(),0)
		self.assertEqual(len(self._memory.getMemory()),30)
		#print(self._memory)
		self.assertEqual(self._p1.getBd(),0)

		program2 = Program("programaTest2", [CPU(7),IO(5)])
		p2 = Pcb(0,0,9,13)
		self._pcbTable.add(p2)
		self._disk.addDatos(program2)
		#print(program2.getSize())
		self._loader.load(program2, p2)
		self.assertEqual(p2.getBd(),6)
		#print(self._memory)

		program3 = Program("programaTest3", [CPU(3),IO(4)])
		p3 = Pcb(0,0,9,8)
		self._pcbTable.add(p3)
		self._disk.addDatos(program3)
		self._loader.load(program3, p3)
		self.assertEqual(p3.getBd(),19)
		#print(self._memory)
		## LIBERAR MEMORIAA

		self._loader.clear( p2)
		p2.setState("TERMINATED")
		#print(self._memory)

		p4 = Pcb(0,0,5,6)
		self._pcbTable.add(p4)
		self._loader.load(self._program1, p4)
		self.assertEqual(p4.getBd(),6)
		print(self._memory)


		program5 = Program("programaTest5", [CPU(4),IO(3)])
		p5 = Pcb(0,0,9,7)
		self._pcbTable.add(p5)
		self._disk.addDatos(program5)
		self._loader.load(program5, p5)
		#memoryManager.compactar(memoryManager.getBloqueInicial())
		#self._loader.actualizarMemoria(18,27,8)
		self.assertEqual(memoryManager.getBloqueInicial().getBd(),29)
		self.assertEqual(memoryManager.getBloqueInicial().getLimite(),29)
		self.assertEqual(p5.getBd(),21)
		self.assertEqual(p4.getBd(),6)
		self.assertEqual(p3.getBd(),12)
		self.assertEqual(self._p1.getBd(),0)

		print(self._memory)

if __name__ == '__main__':
	unittest.main()