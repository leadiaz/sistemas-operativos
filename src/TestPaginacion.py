import unittest
from Bloque import *
from MemoryManager import *
from Programa import *
from Instruccion import *
from Memory import *
from Mmu import *
from Pcb import *
from Loader import *
from Disk import *
class TestPaginacion(unittest.TestCase):

    def setUp(self):
        self._memory = Memory(32)

        """self._p = Program("test.exe", [CPU(5), IO(1), CPU(3)])
        self._p1 = Program("test2.exe", [CPU(3), IO(1)])
        self._p2 = Program("test3.exe", [CPU(4)])
        self._disco = Disk()
        self._disco.addDatos(self._p)
        self._disco.addDatos(self._p1)
        self._disco.addDatos(self._p2)"""
        self._loader = Loader(self._memory, None, None)
        self._paginacion = Paginacion(4,self._memory, self._loader, None)
        self._p = Program("test.exe", [CPU(3),IO(2)])
        self._p2 = Program("test2.exe",[CPU(4),IO(6),CPU(4)])
        self._mmu = MMUPaginacion(self._memory,self._paginacion.getPaginas(), self._paginacion.tamanhoDePaginas())
        self._loader.setMmu(self._mmu)


    def testBuscarPaginas(self):
        pag =self._paginacion.buscarMarcos(self._paginacion.calcularPaginas(self._p))
        self.assertEqual(len(pag), 2)
        self.assertEqual(pag[1].getBd(), 4)
        self.assertTrue(self._paginacion.puedeCargar(self._p2))
        #pag2 = self._paginacion.buscarMarcos(self._paginacion.calcularPaginas(self._p2))

    def testCargarPrograma(self):
        p1 = Pcb(0, 0, 9, 6)
        p2 = Pcb(1, 0, 9, 15)
        self._paginacion.load(self._p, p1)
        print (self._memory)
        self.assertTrue(self._paginacion.puedeCargar(self._p2))
        self._paginacion.load(self._p2, p2)
        print(self._memory)
        self.assertEqual(len(p1.getTablaDePagina()), 2)
        self.assertEqual(len(p2.getTablaDePagina()), 4)
        self.assertEqual(self._paginacion.getPaginasLibres(),2)

    def testTerminarProceso(self):

        self.assertEqual(self._paginacion.getPaginasLibres(), 8)
        p1 = Pcb(0, 0, 9, 6)
        p2 = Pcb(1, 0, 9, 15)
        self._paginacion.load(self._p, p1)
        print (self._memory)
        self.assertEqual(self._paginacion.getPaginasLibres(), 6)
        self.assertTrue(self._paginacion.puedeCargar(self._p2))
        self._paginacion.load(self._p2, p2)
        print(self._memory)

        self.assertEqual(len(p1.getTablaDePagina()), 2)
        self.assertEqual(len(p2.getTablaDePagina()), 4)
        self._paginacion.liberarMemoria(p1)
        print (self._memory)
        self.assertEqual(self._paginacion.getPaginasLibres(), 2)





if __name__ == '__main__':
    unittest.main()