import unittest
from Memory import *
from Programa import *
from Instruccion import*
from Cpu import *
from Scheduler import *
from Clock import *
from Memory import *
from Loader import *
from InterruptManager import *
from IODevice import *
from Disk import *
from Mmu import *
from PcbTable import *
from Dispatcher import *
from MemoryManager import *
from BuscarBloque import *
class TestSO1(unittest.TestCase):

	def setUp(self):
		self._ioDevice = IODevice()
		self._p = Program("test.exe", [CPU(5), IO(1), CPU(3)])
		self._p1 = Program("test2.exe", [CPU(3),IO(1)])
		self._p2 = Program("test3.exe", [CPU(4)])
		self._disco = Disk()
		self._disco.addDatos(self._p)
		self._disco.addDatos(self._p1)
		self._disco.addDatos(self._p2)
		self._memory = Memory(32)
		self._cpu = Cpu()
		self._ioDevice = IODevice()
		self._scheduler = FCFS()
		self._mmu = Mmu(self._memory)
		self._loader = Loader(self._memory,self._disco,self._mmu)
		self._pcbTable = PcbTable()
		self._dispatcher = Dispatcher()
		bloque = Bloque(0,31)
		self._memoryMAnager = AsignacionContinua(FirstFit(), self._memory, bloque, 31, self._loader)
		self._dispatcher.setMmu(self._mmu)
		self._interruptManager = InterruptManager( self._pcbTable, self._loader)
		self._interruptManager.setCpu(self._cpu)
		self._interruptManager.setDispatcher(self._dispatcher)
		self._interruptManager.setScheduler(self._scheduler)
		self._interruptManager.setIoDevice(self._ioDevice)
		self._cpu.setInterruptManager(self._interruptManager)
		self._cpu.setMemory(self._memory)
		self._cpu.setMmu(self._mmu)
		self._ioDevice.setInterrumpManager(self._interruptManager)
		self._dispatcher.setCpu(self._cpu)
		self._loader.setMemoryManager(self._memoryMAnager)
		self._clock = Clock( self._cpu, self._ioDevice)
		self._clock.setSchedulerRoundRobin(self._scheduler)
		self._so = SO(self._memory,self._cpu, self._clock, self._disco, self._interruptManager)
		self._interruptManager.setMemorymanager(self._memoryMAnager)

	def testSO2(self):
		self._so.run("test.exe")
		self._so.run("test2.exe")
		self._so.run("test3.exe")
		print(self._cpu.getPc())
		print(self._memory)
		self._so.clock.tick()
		print(self._dispatcher.pcbActual())
		print(self._cpu)
		self._so.clock.tick()
		print(self._cpu)
		self._so.clock.tick()
		print(self._cpu)
		self._so.clock.tick()
		print(self._cpu)
		self._so.clock.tick()
		print(self._cpu)
		self._so.clock.tick()
		print(self._dispatcher.pcbActual())
		print(self._cpu)
		self._so.clock.tick()
		print(self._cpu)
		self._so.clock.tick()
		print(self._cpu)
		self._so.clock.tick()
		print(self._cpu)
		self._so.clock.tick()
		print(self._cpu)

		self._so.clock.tick()
		print(self._cpu)

		self._so.clock.tick()
		print(self._cpu)


		self._so.clock.tick()
		print(self._cpu)

		self._so.clock.tick()
		print(self._cpu)
		#print(self._memory)

		self._so.clock.tick()
		print(self._cpu)

		self._so.clock.tick()
		print(self._cpu)
		#print(self._memory)

		self._so.clock.tick()
		print(self._cpu)

		self._so.clock.tick()
		print(self._cpu)

		self._so.clock.tick()
		print(self._cpu)

		#print(self._memory)

		self._so.clock.tick()
		print(self._cpu)
		#print(self._memory)
if __name__ == '__main__':
	unittest.main()					