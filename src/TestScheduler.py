import unittest
from Scheduler import *
from Pcb import *
class TestScheduler(unittest.TestCase):
	"""docstring for TestScheduler"""
	def setUp(self):
		self._scheduler = FCFS()
		self._prioridad = Prioridad()
		self._sjf  =SJF()
		self._roundRobin = RoundRobin(3)
		self._scheduleExpropiativo = SJFExpropiativo()
		#self._roundRobin.setearQuantum(3)
		self.p = Pcb(0,0,9,25)
		self.p1 = Pcb (1,5, 8,56)
		self.p2 = Pcb (2,8, 2,10)

	def testAddScheduler(self):
		self._scheduler.add(self.p)
		self._scheduler.add(self.p1)

		self.assertEqual(len(self._scheduler.colaReady()), 2)

	def testGetNextFCFS(self):
		self._scheduler.add(self.p)
		self._scheduler.add(self.p1)

		self.assertEqual(self._scheduler.getNext().id(), self.p.id())
		self.assertEqual(len(self._scheduler.colaReady()), 1)

	def testGetNextPrioridad(self):
		self._prioridad.add(self.p1)
		self._prioridad.add(self.p2)
		self._prioridad.add(self.p)
		self.assertEqual(self._prioridad.getNext().id(), self.p.id())

	def testGetNextSJF(self):
		self._sjf.add(self.p1)
		self._sjf.add(self.p2)
		self._sjf.add(self.p)
		p= self._sjf.getNext()
		self.assertEqual(p.id(), self.p2.id())
		self.assertEqual(p.size(), 10)
	def testCompararSJFExpropiativo(self):
		self._sjf.add(self.p1)
		self._sjf.add(self.p2)
		self._sjf.add(self.p)	

	def testRoundRobin(self):
		self._roundRobin.add(self.p1)
		self._roundRobin.add(self.p2)
		self._roundRobin.add(self.p)
		self._roundRobin.tickParaTestear()
		self.assertEqual(len(self._roundRobin.colaReady()), 3)
		self._roundRobin.tickParaTestear()
		self._roundRobin.tickParaTestear()
		## al hacer tres tick la cpu saca el proceso activo actual y coloca en su lugar al proximo en la cola de ready
		self.assertEqual(len(self._roundRobin.colaReady()), 2)



if __name__ == '__main__':
	unittest.main()