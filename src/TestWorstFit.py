import unittest
from BuscarBloque import *
from Bloque import *
class TestWorstFit(unittest.TestCase):


    def setUp(self):
        self._buscador = WorstFit()
        self._bloque1  = Bloque(0, 12)
        self._bloque2 = Bloque(23, 52)
        self._bloque3 = Bloque(60, 70)
        self._bloque4 = Bloque(100, 200)

        self._bloque1.setNextBloque(self._bloque2)
        self._bloque2.setNextBloque(self._bloque3)
        self._bloque3.setNextBloque(self._bloque4)

    def testGetBloque(self):
        print (self._bloque1.size())
        print (self._bloque2.size())
        print (self._bloque3.size())
        miBloque = self._buscador.getBloque(self._bloque1, 12)
        print (miBloque.size())
        self.assertEqual(miBloque, self._bloque4)
        self.assertEqual(miBloque.size(), 100)


if __name__ == '__main__':
    unittest.main()