#!/usr/bin/env python

#from tabulate import tabulate
from time import sleep
import logging
from Memory import *
from Programa import *
from Instruccion import*
from Cpu import *
from Scheduler import *
from Clock import *
from Memory import *
from Loader import *
from InterruptManager import *
from IODevice import *
from Disk import *
from Mmu import *
from PcbTable import *
from Dispatcher import *
from MemoryManager import *
from BuscarBloque import *

class SO():
    def __init__(self, memory, cpu, clock, disco, interruptManager):
        self._memory = memory
        self._cpu = cpu
        self._clock = clock
        self._disco = disco
        self._interruptManager = interruptManager
    def executeAll(self, programas):
        for p in programas:
            self.execute(p)

    def execute(self, prog):
        self._memory.load(prog)
        logger.debug(self)
        self._cpu.pc = 0

       ## run all program instructions
        inst_count = len(prog.instructions)
        for num in range(0, inst_count):
          self._cpu.tick()

    def run(self, name):
        p = self._disco.getProgram(name)
        self._interruptManager.new(p,3)

    @property
    def cpu(self):
        return self._cpu

    @property
    def clock(self):
        return self._clock

    def __repr__(self):
        return "{cpu}\n{mem}".format(cpu=self._cpu, mem=self._memory)


if __name__ == '__main__':

    ## Configure Logger
    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    logger.info('Starting emulator')


    p = Program("test.exe", [CPU(5), IO(2), CPU(3)])
    p1 = Program("test2.exe", [CPU(3),IO(2)])



    disco = Disk()
    disco.addDatos(p)
    disco.addDatos(p1)
    memory = Memory(32)
    cpu = Cpu()
    ioDevice = IODevice()
    scheduler = FCFS()
    mmu = Mmu(memory)
    loader = Loader(memory,disco, mmu)
    pcbTable = PcbTable()
    dispatcher = Dispatcher()
    bloque = Bloque(0,64)
    memoryMAnager = MemoryManager(FirstFit(), memory, bloque, 64, loader)

    dispatcher.setMmu(mmu)
    interruptManager = InterruptManager( pcbTable, loader)
    interruptManager.setCpu(cpu)
    interruptManager.setDispatcher(dispatcher)
    interruptManager.setScheduler(scheduler)
    interruptManager.setIoDevice(ioDevice)

    cpu.setInterruptManager(interruptManager)
    cpu.setMemory(memory)
    ioDevice.setInterrumpManager(interruptManager)

    dispatcher.setCpu(cpu)


    loader.setMemoryManager(memoryMAnager)

    
    clock = Clock( cpu, ioDevice)
    so = SO(memory,cpu, clock, disco, interruptManager)
    #so.executeAll([p,p1])
    so.run("test.exe")
    so.clock.tick()
    so.clock.tick()
    so.clock.tick()
    so.clock.tick()
    so.clock.tick()
    so.clock.tick()
    so.clock.tick()
    so.run("test2.exe")
    so.clock.tick()
    so.clock.tick()
    so.clock.tick()
    so.clock.tick()
    so.clock.tick()
    so.clock.tick()

