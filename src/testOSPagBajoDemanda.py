import unittest
from Memory import *
from Programa import *
from Instruccion import *
from Cpu import *
from Scheduler import *
from Clock import *
from Memory import *
from Loader import *
from InterruptManager import *
from IODevice import *
from Disk import *
from Mmu import *
from PcbTable import *
from Dispatcher import *
from MemoryManager import *
from BuscarBloque import *


class TestSOPagBajoDemanda(unittest.TestCase):
    def setUp(self):
        self._ioDevice = IODevice()
        self._p = Program("test.exe", [CPU(5), IO(1), CPU(3)])
        self._p1 = Program("test2.exe", [CPU(3), IO(1)])
        self._p2 = Program("test3.exe", [CPU(4)])
        self._p3 = Program("test4.exe", [CPU(4)])
        self._p4 = Program("test5.exe", [CPU(2)])
        self._disco = Disk()
        self._disco.addDatos(self._p)
        self._disco.addDatos(self._p1)
        self._disco.addDatos(self._p2)
        self._disco.addDatos(self._p3)
        self._disco.addDatos(self._p4)
        self._memory = Memory(32)
        self._cpu = Cpu()
        self._cpu.setearEsPaginacion(True)
        self._ioDevice = IODevice()
        self._scheduler = RoundRobin(3)
        self._scheduler.setEsPaginacion(True)
        self._mmu = MMUPaginacionBajoDemanda(self._memory, None, None)
        self._loader = Loader(self._memory, self._disco, self._mmu)
        self._pcbTable = PcbTable()
        self._dispatcher = Dispatcher()
        bloque = Bloque(0, 31)
        self._memoryMAnager = PaginacionBajoDemanda(4,self._memory, self._loader, self._pcbTable, SelectVictim())
        self._dispatcher.setMmu(self._mmu)
        self._interruptManager = InterruptManager(self._pcbTable, self._loader)
        self._interruptManager.setCpu(self._cpu)
        self._interruptManager.setDispatcher(self._dispatcher)
        self._interruptManager.setScheduler(self._scheduler)
        self._interruptManager.setIoDevice(self._ioDevice)
        self._cpu.setInterruptManager(self._interruptManager)
        self._cpu.setMemory(self._memory)
        self._cpu.setMmu(self._mmu)
        self._ioDevice.setInterrumpManager(self._interruptManager)
        self._dispatcher.setCpu(self._cpu)
        self._loader.setMemoryManager(self._memoryMAnager)
        self._clock = Clock(self._cpu, self._ioDevice)
        self._clock.setSchedulerRoundRobin(self._scheduler)
        self._so = SO(self._memory, self._cpu, self._clock, self._disco, self._interruptManager)
        self._scheduler.setInterrupt(self._interruptManager)
        self._interruptManager.setMemorymanager(self._memoryMAnager)
        self._mmu.setInterruptManager(self._interruptManager)
        self._mmu.setTablaDePaginas(self._memoryMAnager.getPaginas())
        self._mmu.setTamanhoDePagina(self._memoryMAnager.tamanhoDePaginas())
    def testSO2(self):
        self._so.run("test.exe")
        self._so.run("test2.exe")
        self._so.run("test3.exe")
        self._so.run("test4.exe")
        print(self._memory)
        print(self._cpu)
        self._so.clock.tick()
        print(self._dispatcher.pcbActual())
        print(self._cpu)
        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)

        self._so.run("test5.exe")  # LLEGA UM NUEVO PROGRAMA

        self._so.clock.tick()
        print(self._cpu)

        self._so.clock.tick()
        print(self._cpu)
        self._so.run("test5.exe")  # LLEGA UM NUEVO PROGRAMA
        self._so.clock.tick()
        print(self._cpu)
        self._so.clock.tick()
        print(self._cpu)


if __name__ == '__main__':
    unittest.main()