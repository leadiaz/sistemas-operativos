import unittest
from Bloque import *
from MemoryManager import *
from Programa import *
from Instruccion import *
from Memory import *
from Mmu import *
from Pcb import *
from Loader import *
from SelectVictim import *
class TestPaginacionBajoDemanda(unittest.TestCase):

    def setUp(self):
        self._memory = Memory(32)
        self._loader = Loader(self._memory, None, None)
        self._paginacion = PaginacionBajoDemanda(4,self._memory, self._loader, None, SelectVictim())
        self._p = Program("test.exe", [CPU(3),IO(2)])
        self._p2 = Program("test2.exe",[CPU(4),IO(6),CPU(4)])
        self._mmu = MMUPaginacion(self._memory,self._paginacion.getPaginas(), self._paginacion.tamanhoDePaginas())
        self._loader.setMmu(self._mmu)

    def testBuscarPaginas(self):
        pag =self._paginacion.buscarMarcos(1)
        pag1 = self._paginacion.buscarMarcos(1)
        pag2= self._paginacion.buscarMarcos(1)
        pag3 = self._paginacion.buscarMarcos(1)
        pag4 = self._paginacion.buscarMarcos(1)
        #self.assertEqual(len(pag), 2)
        print (pag)
        self.assertEqual(pag.getBd(), 0)
        self.assertEqual(pag1.getBd(), 4)
        self.assertEqual(pag2.getBd(), 8)
        self.assertEqual(pag3.getBd(), 12)
        self.assertEqual(pag4.getBd(), 16)


    def testCargarPrograma(self):
        p1 = Pcb(0, 0, 9, 6)
        p2 = Pcb(1, 0, 9, 15)
        self._paginacion.load(self._p, p1)
        print (self._memory)
        self._paginacion.load(self._p2, p2)
        print(self._memory)
        self.assertEqual(p1.getTablaDePagina()[0].getBd(), 0)
        self.assertEqual(p2.getTablaDePagina()[0].getBd(), 4)
        #self.assertEqual(self._paginacion.getPaginasLibres(),6)

    def testTerminarProceso(self):

        self.assertEqual(self._paginacion.getPaginasLibres(), 8)
        p1 = Pcb(0, 0, 9, 6)
        p2 = Pcb(1, 0, 9, 15)
        self._paginacion.load(self._p, p1)
        print (self._memory)
        self.assertTrue(self._paginacion.puedeCargar(self._p2))
        self._paginacion.load(self._p2, p2)
        print(self._memory)

        self.assertEqual(p1.getTablaDePagina()[0].getBd(), 0)
        self.assertEqual(p2.getTablaDePagina()[0].getBd(), 4)
        self._paginacion.liberarMemoria(p1)
        print (self._memory)





if __name__ == '__main__':
    unittest.main()